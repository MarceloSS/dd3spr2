import {
  BrowserRouter as Router,
  useHistory,
  Switch,
  Route,
  Link
} from "react-router-dom";
import {useState} from 'react';
import './App.css';

function App() {
  const [access, setAccess] = useState(false);

  const handleToggle = () => {
    setAccess(!access)
  }  

  return (
    <div className="App">
      <header className="App-header">

        <ul>
          <li>
            <Link to="/">Home</Link>
            <span> | </span>
            <Link to="/restricted">Restricted</Link>
          </li>
        </ul>

        <Switch>
          <Route exact path="/">
            <div>Hello!!!</div>
            <button onClick={handleToggle}>Access</button>
          </Route>
          <Route path="/restricted">
            {!access ? 
            <div>Sorry, this area is restricted</div>
            :
            <div>Welcome</div>
          }
          </Route>
        </Switch>


      </header>
    </div>
  );
}

export default App;
